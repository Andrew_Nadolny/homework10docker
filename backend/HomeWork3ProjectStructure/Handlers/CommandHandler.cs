﻿using AutoMapper;
using Common.DTO;
using Common.Models;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Interfaces;
using System;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Handlers
{
    public class CommandHandler : ICommandHandler<CreateProjectCommand, Task<ProjectDTO>>,
          ICommandHandler<CreateTaskCommand, Task<TaskDTO>>,
          ICommandHandler<CreateTeamCommand, Task<TeamDTO>>,
          ICommandHandler<CreateUserCommand, Task<UserDTO>>,
          ICommandHandler<UpdateProjectCommand, Task<ProjectDTO>>,
          ICommandHandler<UpdateTaskCommand, Task<TaskDTO>>,
          ICommandHandler<UpdateTeamCommand, Task<TeamDTO>>,
          ICommandHandler<UpdateUserCommand, Task<UserDTO>>,
          ICommandHandler<DeleteProjectByIdCommand, Task<bool>>,
          ICommandHandler<DeleteTaskByIdCommand, Task<bool>>,
          ICommandHandler<DeleteTeamByIdCommand, Task<bool>>,
          ICommandHandler<DeleteUserByIdCommand, Task<bool>>,
          ICommandHandler<DeleteProjectByItemCommand, Task<bool>>,
          ICommandHandler<DeleteTaskByItemCommand, Task<bool>>,
          ICommandHandler<DeleteTeamtByItemCommand, Task<bool>>,
          ICommandHandler<DeleteUserByItemCommand, Task<bool>>

    {
        private readonly HomeWork3ProjectStructure.UnitOfWork.UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CommandHandler(ProjectsDbContext context, IMapper mapper)
        {
            _unitOfWork = new UnitOfWork.UnitOfWork(context);
            _mapper = mapper;
        }

        public async Task<ProjectDTO> HandlerAsync(CreateProjectCommand command)
        {
            if (command.project.Id != 0)
            {
                throw new ArgumentException();
            }
            command.project.CreatedAt = DateTime.Now;
            var project = await _unitOfWork.Set<Project>().CreateAsync(_mapper.Map<Project>(command.project));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<TaskDTO> HandlerAsync(CreateTaskCommand command)
        {
            command.task.CreatedAt = DateTime.Now;
            var task = await _unitOfWork.Set<Common.Models.Task>().CreateAsync(_mapper.Map<Common.Models.Task>(command.task));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<TeamDTO> HandlerAsync(CreateTeamCommand command)
        {
            if (command.team.Id != 0 || (await _unitOfWork.Set<Team>().GetAsync(command.team.Id)) != null)
            {
                throw new ArgumentException();
            }
            command.team.CreatedAt = DateTime.Now;
            var team = await _unitOfWork.Set<Team>().CreateAsync(_mapper.Map<Team>(command.team));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<UserDTO> HandlerAsync(CreateUserCommand command)
        {
            if ((await _unitOfWork.Set<User>().GetAsync(command.user.Id)) != null)
            {
                throw new ArgumentException();
            }
            if (command.user.TeamId < 0 || (await _unitOfWork.Set<Team>().GetAsync((int)command.user.TeamId)) == null)
            {
                throw new ArgumentException();
            }
            command.user.RegisteredAt = DateTime.Now;
            User user = await _unitOfWork.Set<User>().CreateAsync(_mapper.Map<User>(command.user));
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }

        public async Task<ProjectDTO> HandlerAsync(UpdateProjectCommand command)
        {
            var project = _mapper.Map<ProjectDTO>(await _unitOfWork.Set<Project>().UpdateAsync(_mapper.Map<Project>(command.project)));
            await _unitOfWork.SaveChangesAsync();
            return project;
        }

        public async Task<TaskDTO> HandlerAsync(UpdateTaskCommand command)
        {
            if (command.task.Id <= 0 || await _unitOfWork.Set<Common.Models.Task>().GetAsync(command.task.Id) == null)
            {
                throw new ArgumentException();
            }
            var task = _mapper.Map<TaskDTO>(await _unitOfWork.Set<Common.Models.Task>().UpdateAsync(_mapper.Map<Common.Models.Task>(command.task)));
            await _unitOfWork.SaveChangesAsync();
            return task;
        }

        public async Task<TeamDTO> HandlerAsync(UpdateTeamCommand command)
        {
            var entity = _mapper.Map<Team>(command.team);
            var searchedTeam = await _unitOfWork.Set<Team>().GetAsync(entity.Id);
            if (searchedTeam != null)
            {
                var team = await _unitOfWork.Set<Team>().UpdateAsync(_mapper.Map<Team>(command.team));
                await _unitOfWork.SaveChangesAsync();
                return _mapper.Map<TeamDTO>(team);
            }
            return null;
        }

        public async Task<UserDTO> HandlerAsync(UpdateUserCommand command)
        {
            try
            {
                var entity = _mapper.Map<User>(command.user);
                var searchedUser = await _unitOfWork.Set<User>().GetAsync(entity.Id);
                if (searchedUser != null)
                {
                    var user = _mapper.Map<UserDTO>(await _unitOfWork.Set<User>().UpdateAsync(entity));
                    await _unitOfWork.SaveChangesAsync();
                    return user;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public async Task<bool> HandlerAsync(DeleteProjectByIdCommand command)
        {
            var isDelete = await _unitOfWork.Set<Project>().DeleteAsync(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTaskByIdCommand command)
        {
            var isDelete = await _unitOfWork.Set<Common.Models.Task>().DeleteAsync(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTeamByIdCommand command)
        {
            var isDelete = await _unitOfWork.Set<Team>().DeleteAsync(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteUserByIdCommand command)
        {
            if (await _unitOfWork.Set<User>().GetAsync(command.Id) == null)
            {
                throw new ArgumentException();
            }
            var isDelete = await _unitOfWork.Set<User>().DeleteAsync(command.Id);
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteProjectByItemCommand command)
        {
            var isDelete = await _unitOfWork.Set<Project>().DeleteAsync(_mapper.Map<Project>(command.project));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTaskByItemCommand command)
        {
            if (await _unitOfWork.Set<Common.Models.Task>().GetAsync(command.task.Id) == null)
            {
                throw new ArgumentException();
            }
            var isDelete = await _unitOfWork.Set<Common.Models.Task>().DeleteAsync(_mapper.Map<Common.Models.Task>(command.task));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteTeamtByItemCommand command)
        {
            var isDelete = await _unitOfWork.Set<User>().DeleteAsync(_mapper.Map<User>(command.team));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }

        public async Task<bool> HandlerAsync(DeleteUserByItemCommand command)
        {
            var isDelete = await _unitOfWork.Set<User>().DeleteAsync(_mapper.Map<User>(command.user));
            await _unitOfWork.SaveChangesAsync();
            return isDelete;
        }
    }
}
