﻿using Common.DTO;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Interfaces;
using HomeWork3ProjectStructure.Queries;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Processors
{
    public class QuerieProcessor : IQuerieProcessor
    {
        public QuerieHandler _querieHandler { get; set; }
        public QuerieProcessor(QuerieHandler querieHandler)
        {
            _querieHandler = querieHandler;
        }

        public async Task<List<ProjectDTO>> ProcessedAsync(GetProjectsQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<TaskDTO>> ProcessedAsync(GetTasksQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
        public async Task<List<TeamDTO>> ProcessedAsync(GetTeamsQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
        public async Task<List<UserDTO>> ProcessedAsync(GetUsersQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
        public async Task<ProjectDTO> ProcessedAsync(GetProjectByIdQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
        public async Task<TeamDTO> ProcessedAsync(GetTeamByIdQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
        public async Task<TaskDTO> ProcessedAsync(GetTaskByIdQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
        public async Task<UserDTO> ProcessedAsync(GetUserByIdQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<Dictionary<int, int>> ProcessedAsync(GetCountTasksInProjectByAuthorIdQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<(int Id, string Name, List<UserDTO> Teammates)>> ProcessedAsync(GetListOfTeamsWithTeamatesOlderThen10Querie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<(UserDTO User, List<TaskDTO> Tasks)>> ProcessedAsync(GetListOfUsersAscendingWhitTasksDescendingQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>> ProcessedAsync(GetProjectWithTaskWithLongestDescriptionsAndETCQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<TaskDTO>> ProcessedAsync(GetUserTasksFinishedInCurrentYearQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<TaskDTO>> ProcessedAsync(GetUserTasksWithShortNameByUserIdQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)>> Processed(GetUserWithLastProjectAndETCQuerie querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }

        public async Task<List<TaskDTO>> ProcessedAsync(GetUserUnfinishedTasks querie)
        {
            return await _querieHandler.HandleAsync(querie);
        }
    }
}
