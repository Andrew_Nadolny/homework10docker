﻿namespace HomeWork3ProjectStructure.Interfaces
{
    interface IQuerieHandler<TQuerie, TResult> where TQuerie : IQuerie<TResult>
    {
        public TResult HandleAsync(TQuerie querie);
    }
}
