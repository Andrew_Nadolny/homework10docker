﻿using AutoMapper;
using Common.DTO;
using HomeWork3ProjectStructure.Commands;
using HomeWork3ProjectStructure.DAL;
using HomeWork3ProjectStructure.Handlers;
using HomeWork3ProjectStructure.Processors;
using HomeWork3ProjectStructure.Queries;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;

// For more information on enabling Web API for empty teams, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HomeWork3TeamStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        // GET: api/<TeamController>
        private readonly QuerieProcessor _querieProcessor;
        private readonly CommandProcessor _commandProcessor;

        public TeamController(ProjectsDbContext context, IMapper mapper)
        {
            _querieProcessor = new QuerieProcessor(new QuerieHandler(context, mapper));
            _commandProcessor = new CommandProcessor(new CommandHandler(context, mapper));

        }
        // GET: api/<TeamController>
        [HttpGet]
        public async System.Threading.Tasks.Task<string> GetAsync()
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTeamsQuerie()));
        }

        // GET api/<TeamController>/5
        [HttpGet("{id}")]
        public async System.Threading.Tasks.Task<string> GetAsync(int id)
        {
            return JsonConvert.SerializeObject(await _querieProcessor.ProcessedAsync(new GetTeamByIdQuerie() { Id = id }));
        }

        // POST api/<TeamController>
        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> PostAsync([FromBody] TeamDTO team)
        {
            try
            {
                return Ok(JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new CreateTeamCommand() { team = team })));
            }
            catch (ArgumentException)
            {
                return BadRequest();
            }
        }

        // PUT api/<TeamController>/5
        [HttpPut]
        public async System.Threading.Tasks.Task<string> Put([FromBody] TeamDTO team)
        {
            return JsonConvert.SerializeObject(await _commandProcessor.ProcessedAsync(new UpdateTeamCommand() { team = team }));
        }

        // DELETE api/<TeamController>/5
        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task<bool> DeleteAsync(int id)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTeamByIdCommand() { Id = id });
        }

        [HttpDelete]
        public async System.Threading.Tasks.Task<bool> Delete([FromBody] TeamDTO team)
        {
            return await _commandProcessor.ProcessedAsync(new DeleteTeamtByItemCommand() { team = team });

        }
    }
}
