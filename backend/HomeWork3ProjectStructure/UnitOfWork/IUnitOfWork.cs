﻿using Common.Models;
using HomeWork3ProjectStructure.Repositores;
using System.Threading.Tasks;


namespace HomeWork3ProjectStructure.UnitOfWork
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Set<TEntity>() where TEntity : Entity;

        Task<int> SaveChangesAsync();
    }
}
