﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetTaskByIdQuerie : IQuerie<Task<TaskDTO>>
    {
        public int Id { get; set; }

    }
}
