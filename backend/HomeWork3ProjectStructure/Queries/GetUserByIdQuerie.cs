﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Queries
{
    public class GetUserByIdQuerie : IQuerie<Task<UserDTO>>
    {
        public int Id { get; set; }
    }
}
