﻿using Common.DTO;
using HomeWork3ProjectStructure.Interfaces;
using System.Threading.Tasks;

namespace HomeWork3ProjectStructure.Commands
{
    public class DeleteTeamtByItemCommand : ICommand<Task<bool>>
    {
        public TeamDTO team { get; set; }
    }
}
