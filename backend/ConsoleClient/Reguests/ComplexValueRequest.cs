﻿using AutoMapper;
using Common.DTO;
using Common.MappingProfiles;
using Common.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace ConsoleClient.Reguests
{
    public class ComplexValueRequest : IDisposable
    {
        public string ApiUrl = "https://localhost:44309";
        private Mapper _mapper;
        HttpClient httpClient;
        public ComplexValueRequest()
        {
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(ApiUrl);
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();

            }));
        }

        public async System.Threading.Tasks.Task<Dictionary<int, int>> GetCountTasksInProjectByAuthorId(int userId)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetCountTasksInProjectByAuthorId?id={0}", userId));
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<Dictionary<int, int>>(await response.Content.ReadAsStringAsync());
            }
            return null;
        }

        //public async System.Threading.Tasks.Task<List<Project>> GetProjectsStructureQuerie()
        //{
        //    HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetProjectsStructureQuerie"));
        //    if (response.IsSuccessStatusCode)
        //    {
        //        return _ JsonConvert.DeserializeObject<List<ProjectDTO>>(await response.Content.ReadAsStringAsync());
        //    }
        //    return null;
        //}


        public async System.Threading.Tasks.Task<List<Task>> GetUserTasksWithShortNameByUserId(int userId)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetUserTasksWithShortNameByUserId?id={0}", userId));
            if (response.IsSuccessStatusCode)
            {
                return _mapper.Map<List<Task>>(JsonConvert.DeserializeObject<List<TaskDTO>>(await response.Content.ReadAsStringAsync()));
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<Task>> GetUserTasksFinishedInCurrentYear(int userId)
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetUserTasksFinishedInCurrentYear?id={0}", userId));
            if (response.IsSuccessStatusCode)
            {
                return _mapper.Map<List<Task>>(JsonConvert.DeserializeObject<List<TaskDTO>>(await response.Content.ReadAsStringAsync()));
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(int Id, string Name, List<User> Teammates)>> GetListOfTeamsWithTeamatesOlderThen10()
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetListOfTeamsWithTeamatesOlderThen10"));
            if (response.IsSuccessStatusCode)
            {
                var responseStructure = JsonConvert.DeserializeObject<List<(int Id, string Name, List<UserDTO> Teammates)>>(await response.Content.ReadAsStringAsync());
                return responseStructure.Select(x => (Id: x.Id, Name: x.Name, Teammates: _mapper.Map<List<User>>(x.Teammates))).ToList();
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(User User, List<Task> Tasks)>> GetListOfUsersAscendingWhitTasksDescending()
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetListOfUsersAscendingWhitTasksDescending"));
            if (response.IsSuccessStatusCode)
            {
                var responseStructure = JsonConvert.DeserializeObject<List<(UserDTO User, List<TaskDTO> Tasks)>>(await response.Content.ReadAsStringAsync());
                return responseStructure.Select(x => (User: _mapper.Map<User>(x.User), Tasks: _mapper.Map<List<Task>>(x.Tasks))).ToList();
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(User User, Project LastProject, List<Task> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, Task LongesUserTask)>>
            GetUserWithLastProjectAndETC()
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetUserWithLastProjectAndETC"));
            if (response.IsSuccessStatusCode)
            {
                var responseStructure = JsonConvert.DeserializeObject<List<(UserDTO User, ProjectDTO LastProject, List<TaskDTO> LastProjectTask, int CountOfUnfinishedAndCanceledTasks, TaskDTO LongesUserTask)>>(await response.Content.ReadAsStringAsync());
                return responseStructure.Select(x => (User: _mapper.Map<User>(x.User), LastProject: _mapper.Map<Project>(x.LastProject), LastProjectTask: _mapper.Map<List<Task>>(x.LastProjectTask), CountOfUnfinishedAndCanceledTasks: x.CountOfUnfinishedAndCanceledTasks, LongesUserTask: _mapper.Map<Task>(x.LongesUserTask))).ToList();
            }
            return null;
        }

        public async System.Threading.Tasks.Task<List<(Project Project, Task TaskWithLongestDescriptions, Task TaskWithShortestName, int CountOfUsersInProjects)>>
            GetProjectWithTaskWithLongestDescriptionsAndETC()
        {
            HttpResponseMessage response = await httpClient.GetAsync(string.Format("/api/ComplexValues/GetProjectWithTaskWithLongestDescriptionsAndETC"));
            if (response.IsSuccessStatusCode)
            {
                var responseStructure = JsonConvert.DeserializeObject<List<(ProjectDTO Project, TaskDTO TaskWithLongestDescriptions, TaskDTO TaskWithShortestName, int CountOfUsersInProjects)>>(await response.Content.ReadAsStringAsync());
                return responseStructure.Select(x => (User: _mapper.Map<Project>(x.Project), TaskWithLongestDescriptions: _mapper.Map<Task>(x.TaskWithLongestDescriptions), TaskWithShortestName: _mapper.Map<Task>(x.TaskWithShortestName), CountOfUsersInProjects: x.CountOfUsersInProjects)).ToList();
            }
            return null;
        }

        public void Dispose()
        {
            httpClient.Dispose();
        }

    }
}
