﻿using System;

namespace Common.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                UserDTO secondUserDTO = (UserDTO)obj;
                if (Id == secondUserDTO.Id
                    && TeamId == secondUserDTO.TeamId
                    && Name == secondUserDTO.Name
                    && Surname == secondUserDTO.Surname
                    && Email == secondUserDTO.Email
                    && RegisteredAt == secondUserDTO.RegisteredAt
                    && BirthDay == secondUserDTO.BirthDay)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
