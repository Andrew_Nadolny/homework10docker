﻿using AutoMapper;
using Common.DTO;
using Common.Models;

namespace Common.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDTO, Task>();
            CreateMap<Task, TaskDTO>();
        }
    }
}
