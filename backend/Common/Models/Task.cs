﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Common.Models
{
    public class Task : Entity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        [MaxLength(100), MinLength(1)]
        public string Name { get; set; }
        [MaxLength(100), MinLength(1)]
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }

        public override string ToString()
        {
            return string.Format("\nTask: [Id:{0}, ProjectId:{1}, Performer:{2}, Name:{3}, Description:{4}, TaskState:{5}, CreatedAt:{6}, FinishedAt:{7} ]\n",
                Id,
                ProjectId,
                PerformerId,
                Name,
                Description,
                State,
                CreatedAt,
                FinishedAt);
        }
    }
}
