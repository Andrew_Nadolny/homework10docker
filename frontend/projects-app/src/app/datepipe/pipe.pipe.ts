import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'datePipe'
})
export class PipePipe implements PipeTransform {

  transform(dateInfo: Date): string {
    let date : Date = new Date(dateInfo);
    let options: Intl.DateTimeFormatOptions = {
      day: "numeric", month: 'long', year: "numeric"
  };
    return date.toLocaleDateString("uk", options);
  }

}
