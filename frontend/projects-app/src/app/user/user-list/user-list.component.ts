import { Component, OnDestroy, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';
import { UserGuard } from '../guard/user.guard';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy, UserGuard {
  userService: UserService;
  subscription!: Subscription;
  constructor(userService: UserService) {
    this.userService = userService;
  }
  users: User[] = [];
  showAddUserForm: boolean = false;
  serverError: string = "";
  public newUser = {} as User;
  public addedUser = {} as User;
  public saved: boolean = true;

  ngOnInit(): void {
    this.subscription = this.userService.getUsers().subscribe((data) => {
      this.users = this.sortUsersArray(data);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public addUser() {
    console.log(this.newUser);
    this.subscription = this.userService.addNewUser(this.newUser).subscribe((data) => {
      this.addedUser = data;
      this.showingAddUserForm();
      if (!this.users.some((x) => x.Id === this.addedUser.Id)) {
        this.users = this.sortUsersArray(this.users.concat(this.addedUser));
        this.saved = true;
      }
    }, (error: Error) => { this.serverError = error.message; });
  }

  public sortUsersArray(array: User[]): User[] {
    return array.sort((a, b) => +new Date(b.RegisteredAt) - +new Date(a.RegisteredAt));
  }
  public showingAddUserForm() {
    this.showAddUserForm = !this.showAddUserForm;

  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
      return confirm("You have unsaved data. Do you want leave this page?");
    }
    else {
      return true;
    }
  }

  change() {
    console.log(this.saved);
    if ((this.newUser.BirthDay == new Date() || this.newUser.BirthDay == undefined)
      && (this.newUser.Email == "" || this.newUser.Email == undefined)
      && (this.newUser.Name == "" || this.newUser.Name == undefined)
      && (this.newUser.Surname == "" || this.newUser.Surname == undefined)
      && (this.newUser.TeamId == 0 || this.newUser.TeamId == undefined)
    ) {
      this.saved = true;
    }
    else {
      this.saved = false;
    }
    console.log(this.saved);
  }
}
