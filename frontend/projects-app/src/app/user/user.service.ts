import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('https://localhost:44309/api/user/', { headers: new HttpHeaders()}).pipe(map((data: any) => {
      return data.map(function (user: User): User {
        return new User(user.TeamId, user.Name, user.Surname, user.Email, user.RegisteredAt, user.BirthDay, user.Id);
      });
    }));
  }

  addNewUser(user : User) { 
    return this.http.post<User>(`https://localhost:44309/api/user/`, user);
  }

  updateUser(user : User) { 
    return this.http.put<User>(`https://localhost:44309/api/user/`, user);
  }

  deleteUser(userId : number) { 
    return this.http.delete<User>(`https://localhost:44309/api/user/${userId}`);
  }
}


