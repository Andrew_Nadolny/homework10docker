import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Project } from '../project';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  projectService: ProjectService;
  subscription!: Subscription;
  constructor(projectService: ProjectService) {
    this.projectService = projectService;
  }
  projects: Project[] = [];
  showAddProjectForm: boolean = false;
  serverError: string = "";
  public newProjectr = {} as Project;
  public addedProject = {} as Project;
  public saved: boolean = true;

  ngOnInit(): void {
    this.subscription = this.projectService.getProjects().subscribe((data) => {
      this.projects = this.sortProjectArray(data);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public addProject() {
    console.log(this.newProjectr);
    this.subscription = this.projectService.addNewProject(this.newProjectr).subscribe((data) => {
      this.addedProject = data;
      this.showingAddProjectForm();
      if (!this.projects.some((x) => x.Id === this.addedProject.Id)) {
        this.projects = this.sortProjectArray(this.projects.concat(this.addedProject));
        this.saved = true;
      }
    }, (error: Error) => { this.serverError = error.message; });
  }

  public sortProjectArray(array: Project[]): Project[] {
    return array.sort((a, b) => +new Date(b.CreatedAt) - +new Date(a.CreatedAt));
  }
  public showingAddProjectForm() {
    this.showAddProjectForm = !this.showAddProjectForm;

  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
      return confirm("You have unsaved data. Do you want leave this page?");
    }
    else {
      return true;
    }
  }

  change() {
    console.log(this.saved);
    if ((this.newProjectr.Deadline == new Date() || this.newProjectr.Deadline == undefined)
      && (this.newProjectr.Description == "" || this.newProjectr.Description == undefined)
      && (this.newProjectr.Name == "" || this.newProjectr.Name == undefined)
      && (this.newProjectr.TeamId == 0 || this.newProjectr.TeamId == undefined)
    ) {
      this.saved = true;
    }
    else {
      this.saved = false;
    }
    console.log(this.saved);
  }
}