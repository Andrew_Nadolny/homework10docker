import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectComponent } from './project/project.component';
import { ProjectListComponent } from './project-list/project-list.component';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { DatepipeModule } from '../datepipe/datepipe.module';


@NgModule({
  declarations: [
    ProjectComponent,
    ProjectListComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    DatepipeModule
  ],
  exports: [ProjectListComponent]
})
export class ProjectModule { }
