import { Component, OnInit, Input } from '@angular/core';
import { ProjectListComponent } from '../project-list/project-list.component';
import { ProjectService } from '../project.service';
import { Project } from '../project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent {
  projectService : ProjectService;
  projectlist : ProjectListComponent;
  @Input() public project!: Project;
  constructor(projectService : ProjectService, projectlist: ProjectListComponent) {
    this.projectService = projectService;
    this.projectlist = projectlist;
   }
  showUpdateProjectForm : boolean = false;
  serverError : string = "";

  public updateProject(){
    this.projectService.updateProject(this.project).subscribe((data)=> {
      this.project = data;
      this.showingUpdateProjectForm();
    }, (error : Error) => {this.serverError = error.message;});
  }

  public showingUpdateProjectForm(){
    this.showUpdateProjectForm = !this.showUpdateProjectForm;
  }

  public deleteProject(){
    console.log(this.project);
    this.projectService.deleteProject(this.project.Id).subscribe((data)=> {
      this.projectlist.projects.splice(this.projectlist.projects.findIndex(x => x.Id === this.project.Id), 1)
    }, (error : Error) => {this.serverError = error.message;});
  }
}

