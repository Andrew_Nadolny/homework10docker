import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Project } from './project';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }

  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>('https://localhost:44309/api/project/', { headers: new HttpHeaders()}).pipe(map((data: any) => {
      return data.map(function (project: Project): Project {
        return new Project(project.Id, project.AuthorId, project.TeamId, project.Name, project.Description, project.Deadline, project.CreatedAt );
      });
    }));
  }

  addNewProject(project : Project) { 
    return this.http.post<Project>(`https://localhost:44309/api/project/`, project);
  }

  updateProject(project : Project) { 
    return this.http.put<Project>(`https://localhost:44309/api/project/`, project);
  }

  deleteProject(projectId : number) { 
    return this.http.delete<Project>(`https://localhost:44309/api/project/${projectId}`);
  }
}

