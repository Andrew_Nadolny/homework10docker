export class Project {
    Id: number;
    AuthorId : number;
    TeamId : number;
    Name : string;
    Description : string;
    Deadline : Date;
    CreatedAt : Date;

    constructor(Id: number,AuthorId : number,
        TeamId : number,
        Name : string,
        Description : string,
        Deadline : Date,
        CreatedAt : Date){
            this.Id = Id;
        this.AuthorId = AuthorId;
        this.TeamId = TeamId;
        this.Name = Name;
        this.Description = Description;
        this.Deadline = Deadline;
        this.CreatedAt = CreatedAt;
    }
}
