import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Team } from '../team';
import { TeamService } from '../team.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit, OnDestroy {
  teamService: TeamService;
  subscription!: Subscription;
  constructor(teamService: TeamService) {
    this.teamService = teamService;
  }
  teams: Team[] = [];
  showAddTeamForm: boolean = false;
  serverError: string = "";
  public newTeam = {} as Team;
  public addedTeam = {} as Team;
  public saved: boolean = true;

  ngOnInit(): void {
    this.subscription = this.teamService.getTeams().subscribe((data) => {
      this.teams = this.sortTeamArray(data);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public addTeam() {
    console.log(this.newTeam);
    this.subscription = this.teamService.addNewTeam(this.newTeam).subscribe((data) => {
      this.addedTeam = data;
      this.showingAddTeamForm();
      if (!this.teams.some((x) => x.Id === this.addedTeam.Id)) {
        this.teams = this.sortTeamArray(this.teams.concat(this.addedTeam));
        this.saved = true;
      }
    }, (error: Error) => { this.serverError = error.message; });
  }

  public sortTeamArray(array: Team[]): Team[] {
    return array.sort((a, b) => +new Date(b.CreatedAt) - +new Date(a.CreatedAt));
  }
  public showingAddTeamForm() {
    this.showAddTeamForm = !this.showAddTeamForm;
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
      return confirm("You have unsaved data. Do you want leave this page?");
    }
    else {
      return true;
    }
  }

  change() {
    console.log(this.saved);
    if ((this.newTeam.Name == "" || this.newTeam.Name == undefined)) {
      this.saved = true;
    }
    else {
      this.saved = false;
    }
    console.log(this.saved);
  }
}
