export class Team {
    Id: number;
    Name: string;
    CreatedAt: Date;

    constructor(Id: number,
        Name: string,
        CreatedAt: Date) {
            this.Id = Id;
            this.Name = Name;
            this.CreatedAt = CreatedAt;
    }
}
