import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Team } from './team';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private http: HttpClient) { }

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>('https://localhost:44309/api/team/', { headers: new HttpHeaders() }).pipe(map((data: any) => {
      return data.map(function (team: Team): Team {
        return new Team(team.Id, team.Name, team.CreatedAt);
      });
    }));
  }

  addNewTeam(team: Team) {
    return this.http.post<Team>(`https://localhost:44309/api/team/`, team);
  }

  updateTeam(team: Team) {
    return this.http.put<Team>(`https://localhost:44309/api/team/`, team);
  }

  deleteTeam(teamId: number) {
    return this.http.delete<Team>(`https://localhost:44309/api/team/${teamId}`);
  }
}

