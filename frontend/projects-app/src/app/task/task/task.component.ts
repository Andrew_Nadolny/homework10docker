import { Component, OnInit, Input } from '@angular/core';
import { TaskListComponent } from '../task-list/task-list.component';
import { TaskService } from '../task.service';
import { Task } from '../task';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  taskService : TaskService;
  tasklist : TaskListComponent;
  @Input() public task!: Task;
  constructor(taskService : TaskService, tasklist: TaskListComponent) {
    this.taskService = taskService;
    this.tasklist = tasklist;
   }
  showUpdateTaskForm : boolean = false;
  serverError : string = "";

  public updateTask(){
    this.taskService.updateTask(this.task).subscribe((data)=> {
      this.task = data;
      this.showingUpdateTaskForm();
    }, (error : Error) => {this.serverError = error.message;});
  }

  public showingUpdateTaskForm(){
    this.showUpdateTaskForm = !this.showUpdateTaskForm;
  }

  public deleteTask(){
    console.log(this.task);
    this.taskService.deleteTask(this.task.Id).subscribe((data)=> {
      this.tasklist.tasks.splice(this.tasklist.tasks.findIndex(x => x.Id === this.task.Id), 1)
    }, (error : Error) => {this.serverError = error.message;});
  }
}
