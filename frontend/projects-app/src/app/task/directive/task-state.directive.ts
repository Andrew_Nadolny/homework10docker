import { HtmlParser } from '@angular/compiler';
import { Directive, ElementRef, Input, SimpleChange, SimpleChanges } from '@angular/core';
import { zip } from 'rxjs';

@Directive({
  selector: '[appTaskState]'
})
export class TaskStateDirective {

  constructor(protected el: ElementRef) {
    
  }
  @Input() value!: number;

  ngOnInit(){
    if(this.value == 0){
      this.el.nativeElement.style.backgroundColor = "orange";
    }
    if(this.value == 1){
      this.el.nativeElement.style.backgroundColor = "yellow";
    }
    if(this.value == 2){
      this.el.nativeElement.style.backgroundColor = "green";
    }
    if(this.value == 3){
      this.el.nativeElement.style.backgroundColor = "red";
    }
  }

  ngOnChanges(){
    if(this.value == 0){
      this.el.nativeElement.style.backgroundColor = "orange";
    }
    if(this.value == 1){
      this.el.nativeElement.style.backgroundColor = "yellow";
    }
    if(this.value == 2){
      this.el.nativeElement.style.backgroundColor = "green";
    }
    if(this.value == 3){
      this.el.nativeElement.style.backgroundColor = "red";
    }
  }
}
