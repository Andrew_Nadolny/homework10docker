import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { TaskService } from '../task.service';
import { Task } from '../task';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy {
  taskService: TaskService;
  subscription!: Subscription;
  constructor(taskService: TaskService) {
    this.taskService = taskService;
  }
  tasks: Task[] = [];
  showAddTaskForm: boolean = false;
  serverError: string = "";
  public newTask = {} as Task;
  public addedTask = {} as Task;
  public saved: boolean = true;

  ngOnInit(): void {
    this.subscription = this.taskService.getTasks().subscribe((data) => {
      this.tasks = this.sortTasksArray(data);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  public addTask() {
    console.log(this.newTask);
    this.subscription = this.taskService.addNewTask(this.newTask).subscribe((data) => {
      this.addedTask = data;
      this.showingAddTaskForm();
      if (!this.tasks.some((x) => x.Id === this.addedTask.Id)) {
        this.tasks = this.sortTasksArray(this.tasks.concat(this.addedTask));
        this.saved = true;
      }
    }, (error: Error) => { this.serverError = error.message; });
  }

  public sortTasksArray(array: Task[]): Task[] {
    return array.sort((a, b) => +new Date(b.CreatedAt) - +new Date(a.CreatedAt));
  }
  public showingAddTaskForm() {
    this.showAddTaskForm = !this.showAddTaskForm;
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (!this.saved) {
      return confirm("You have unsaved data. Do you want leave this page?");
    }
    else {
      return true;
    }
  }

  change() {
    console.log(this.saved);
    if ((this.newTask.FinishedAt == new Date() || this.newTask.FinishedAt == undefined)
      && (this.newTask.Description == "" || this.newTask.Description == undefined)
      && (this.newTask.Name == "" || this.newTask.Name == undefined)
      && (this.newTask.PerformerId == 0 || this.newTask.PerformerId == undefined)
      && (this.newTask.ProjectId == 0 || this.newTask.ProjectId == undefined)
      && (this.newTask.State == 0 || this.newTask.State == undefined)
    ) {
      this.saved = true;
    }
    else {
      this.saved = false;
    }
    console.log(this.saved);
  }
}
