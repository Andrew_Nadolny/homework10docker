import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskComponent } from './task/task.component';
import { TaskListComponent } from './task-list/task-list.component';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule } from '@angular/forms';
import { TaskStateDirective } from './directive/task-state.directive';
import { DatepipeModule } from '../datepipe/datepipe.module';



@NgModule({
  declarations: [
    TaskComponent,
    TaskListComponent,
    TaskStateDirective
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    DatepipeModule
  ],
  exports: [TaskListComponent]
})
export class TaskModule { }
